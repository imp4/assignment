Feature: List Creation

Test case creation for creating lists

@mytag
Scenario: Add Lists
Given I have logged into the Trello site
And I have navigated to the Boards Page
When I press add list
Then I should see the List creation dialog to enter title

@mytag
Scenario: Add List with name
Given I have landed into the Boards Page
And I have entered list name into the dialog
When I press Add List button
Then the list should be added on the Page and verify the title of the list

