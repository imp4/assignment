# Testing / Assignment

## About / Synopsis

* This Project tells us how to do execute Test Scripts from locally
* Project status: working/prototype
* Web Support

See real examples:

* <https://github.com/nuxeo/nuxeo-drive/blob/master/README.md>

## Table of contents


> * [Title / Repository Name](#Assignment)
>   * [About / Synopsis](#Execution of test scripts)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>   * [Pre-requisites]
>   * [Test Script Execution]

Installation:

1. Download Git from the given url : https://git-scm.com/download/win
2. Install the Git from the downloaded exe file. 
3. Create a folder from your local driver 
4. Open the folder and right click on anywhere inside the folder it shows you the option to open Git Bash Here. You need to open the Git Bash Command prompt window to get the latest code
5. Now enter the command 'git clone https://gitlab.com/imp4/assignment.git' to download the latest repository
6. Go to the folder where you have created in step 3 
7. Verify there are 5 files gets downloaded inside the folder


Pre-requisites:

1. Require Eclipse Juno or Kepler
2. Download Text Editor like Notepad++
3. Requires Java
4. Nice to have Adobe PDFReader
5. Install Eclipse with added external libraries which supports TestNG Framework(Please follow the below link to create TestNG Project 
https://www.lambdatest.com/blogcreate-testng-project-in-eclipse-run-selenium-test-script/)

Test Script Execution:

1. Open Eclipse 
2. Create Workspace 
3. Import Java file
4. Run as TestNG file
5. View the results
6. Attached the video of the Test Execution


