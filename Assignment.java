package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new ChromeDriver();    // If your machine have chrome browser installed
    baseUrl = "https://www.google.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.manage().window().maximize();
  }

  @Test
  public void testAssignment() throws Exception {
    driver.get("https://trello.com/b/PdJl5nm3/kanban#");
    driver.findElement(By.cssSelector("[href^='/login']")).click();
    driver.findElement(By.id("user")).sendKeys("irsatheng@gmail.com");
    driver.findElement(By.id("login")).click();
    Thread.sleep(5000);
    driver.findElement(By.id("password")).sendKeys("17Sep@1989");
    driver.findElement(By.cssSelector("#login-submit")).click();
    Thread.sleep(20000);
    driver.findElement(By.cssSelector("[class$='js-open-add-list']")).click();
    driver.findElement(By.xpath("//input[@placeholder='Enter list title...']")).sendKeys("Test List");//Enter List in the text box
    driver.findElement(By.xpath("//input[@value='Add List']")).click();
    String EnteredList = "Test List";
    WebElement CreatedList = driver.findElement(By.xpath("//*[@id='board']/div[2]/div/div[1]/div[1]"));
    Assert.assertEquals(EnteredList, CreatedList.getText());
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
